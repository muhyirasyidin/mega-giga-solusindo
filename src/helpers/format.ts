export const noGenerator = (offset: number, index: number) => {
	return (offset - 1) * 10 + index + 1;
}

export const currency = (number: number) => {
  const isNegative = number < 0;
  const absoluteNumber = Math.abs(number);
  const parts = absoluteNumber.toFixed(2).split(".");

  let integerPart = parts[0];
  integerPart = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");

  const decimalPart = parts[1] ? `,${parts[1]}` : "";
  const formattedNumber = `${integerPart}${decimalPart}`;
	
  const currency = isNegative ? `-${formattedNumber}` : formattedNumber;
  return `Rp ${currency}`;
}
