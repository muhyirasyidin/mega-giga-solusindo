import { createStore } from 'vuex'
import createPersistedState from 'vuex-plugin-persistedstate';

/*
	Import Modules
*/
import auth from './modules/auth';
import barang from './modules/barang';
import supplier from './modules/supplier';

export default createStore({
	modules: {
		auth,
		barang,
		supplier
	},
	plugins: [
    createPersistedState()
  ]
})