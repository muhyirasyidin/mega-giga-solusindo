import queryString from 'query-string';

/*
	Apis & Interfaces
*/
import APIS from '@/apis/index';

import type { SuplierTypeModel, SupplierModel } from '@/interfaces/supplier';
import type { PayloadUpdateTypeModel } from '@/interfaces/common';

export default {
	namespaced: true,
	state: {
		supplierParams: {
			offset: 1,
			limit: 10,
			search: ''
		},
		supplier: [],
		supplierPagination: {
			total_page: 1,
			total_record: 0
		},
		selectedSupplier: null,
	},
	mutations: {
		SET_SUPPLIER(state: SuplierTypeModel, data: any) {
			state.supplier = data.data;
			state.supplierPagination = {
				total_page: data.total_page,
				total_record: data.total_record
			}
		},
		SET_PARAMS_PAGE(state: SuplierTypeModel, page: number) {
			state.supplierParams.offset = page;
		},
		SET_PARAMS_LIMIT(state: SuplierTypeModel, limit: number) {
			state.supplierParams.limit = limit;
		},
		SET_SELECTED_SUPPLIER(state: SuplierTypeModel, data: SupplierModel) {
			state.selectedSupplier = data;
		}
	},
	actions: {
		async GET_SUPPLIER(
			{commit, state}: {commit: any, state: SuplierTypeModel}
		) {
			const params = queryString.stringify(state.supplierParams)
			return APIS.supplier.GET_SUPPLIER(params).then(({data}) => {
				commit('SET_SUPPLIER', data);
			}).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		},
		async GET_SUPPLIER_BY_ID({commit}: {commit: any}, credentials: number) {
			return APIS.supplier.GET_SUPPLIER_BY_ID(credentials).then(({data}) => {
				commit('SET_SELECTED_SUPPLIER', data.data);
			}).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		},
		async CREATE_SUPPLIER({commit}: {commit: any}, credentials: object) {
			return APIS.supplier.CREATE_SUPPLIER(credentials).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		},
		async UPDATE_SUPPLIER({commit}: {commit: any}, credentials: PayloadUpdateTypeModel) {
			return APIS.supplier.UPDATE_SUPPLIER(credentials).catch((err) => {
				console.log('err => ', err);
				return err;
			});;
		},
		async DELETE_SUPPLIER({commit}: {commit: any}, credentials: number) {
			return APIS.supplier.DELETE_SUPPLIER(credentials).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		}
	},
	getters: {
		getSupplier: (state: SuplierTypeModel) => state.supplier,
		getSupplierPagination: (state: SuplierTypeModel) => state.supplierPagination
	},
}