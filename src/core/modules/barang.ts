import queryString from 'query-string';

/*
	Apis & Interfaces
*/
import APIS from '@/apis/index';

import type { BarangTypeModel, BarangModel } from '@/interfaces/barang';
import type { PayloadUpdateTypeModel } from '@/interfaces/common';

export default {
	namespaced: true,
	state: {
		barangParams: {
			offset: 1,
			limit: 10,
			search: ''
		},
		barang: [],
		barangPagination: {
			total_page: 1,
			total_record: 0
		},
		selectedBarang: null
	},
	mutations: {
		SET_BARANG(state: BarangTypeModel, data: any) {
			state.barang = data.data;
			state.barangPagination = {
				total_page: data.total_page,
				total_record: data.total_record
			}
		},
		SET_PARAMS_PAGE(state: BarangTypeModel, page: number) {
			state.barangParams.offset = page
		},
		SET_SELECTED_BARANG(state: BarangTypeModel, data: BarangModel) {
			state.selectedBarang = data;
		}
	},
	actions: {
		async GET_BARANG(
			{commit, state}: {commit: any, state: BarangTypeModel}
		) {
			const params = queryString.stringify(state.barangParams)
			return APIS.barang.GET_BARANG(params).then(({data}) => {
				commit('SET_BARANG', data);
				return data;
			}).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		},
		async GET_BARANG_BY_ID({commit}: {commit: any}, credentials: number) {
			return APIS.barang.GET_BARANG_BY_ID(credentials).then(({data}) => {
				commit('SET_SELECTED_BARANG', data.data);
				return data;
			}).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		},
		async CREATE_BARANG({commit}: {commit: any}, credentials: object) {
			return APIS.barang.CREATE_BARANG(credentials).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		},
		async UPDATE_BARANG({commit}: {commit: any}, credentials: PayloadUpdateTypeModel) {
			return APIS.barang.UPDATE_BARANG(credentials).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		},
		async DELETE_BARANG({commit}: {commit: any}, credentials: number) {
			return APIS.barang.DELETE_BARANG(credentials).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		}
	},
	getters: {
		getBarang: (state: BarangTypeModel) => state.barang,
		getBarangPagination: (state: BarangTypeModel) => state.barangPagination
	},
}