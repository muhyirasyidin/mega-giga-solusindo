/*
	Apis & Interfaces
*/
import APIS from '@/apis/index';

import type { AuthTypeModel, UserTypeModel } from '@/interfaces/auth';

export default {
	namespaced: true,
	state: {
		user: null
	},
	mutations: {
		SET_USER(state: AuthTypeModel, data: UserTypeModel) {
			state.user = data;
		},
		RESET_USER(state: AuthTypeModel) {
			state.user = null;
		}
	},
	actions: {
		async LOGIN({commit}: {commit: any}, credential: object) {
			return APIS.auth.LOGIN(credential).then((resp) => {
				commit('SET_USER', resp.data.data);
				return resp;
			}).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		},
		async REGISTER({commit}: {commit: any}, credential: object) {
			return APIS.auth.REGISTER(credential).catch((err) => {
				console.log('err => ', err);
				return err;
			});
		},
		async LOGOUT({commit}: {commit: any}) {
			commit('RESET_USER');
			localStorage.removeItem('VUEX-PERSISTEDSTATE');
			location.reload();
		}
	},
	getters: {
		// getWord: (state: HomeTypeModel) => state.word,
	},
}