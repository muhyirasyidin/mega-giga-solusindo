import axios from 'axios';

/**
 * Interfaces
 */
import type { PayloadUpdateTypeModel } from '@/interfaces/common';

const barang = {
	GET_BARANG: (params: string) => {
		return axios.get(`barang/find-all?${params}`);
	},
	GET_BARANG_BY_ID: (id: number) => {
		return axios.get(`barang/find-by-id/${id}`);
	},
	CREATE_BARANG: (payload: object) => {
		return axios.post('barang/create', payload);
	},
	UPDATE_BARANG: (payload: PayloadUpdateTypeModel) => {
		return axios.put(`barang/update/${payload.id}`, payload.data);
	},
	DELETE_BARANG: (id: number) => {
		return axios.delete(`barang/delete/${id}`);
	}
}

export default barang;