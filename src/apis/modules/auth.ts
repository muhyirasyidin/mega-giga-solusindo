import axios from 'axios';

const auth = {
	LOGIN: (payload: object) => {
		return axios.post('auth/login', payload);
	},
	REGISTER: (payload: object) => {
		return axios.post('auth/register', payload);
	}
}

export default auth;