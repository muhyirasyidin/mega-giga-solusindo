import axios from 'axios';

/**
 * Interfaces
 */
import type { PayloadUpdateTypeModel } from '@/interfaces/common';

const supplier = {
	GET_SUPPLIER: (params: string) => {
		return axios.get(`supplier/find-all?${params}`);
	},
	GET_SUPPLIER_BY_ID: (id: number) => {
		return axios.get(`supplier/find-by-id/${id}`);
	},
	CREATE_SUPPLIER: (payload: object) => {
		return axios.post('supplier/create', payload);
	},
	UPDATE_SUPPLIER: (payload: PayloadUpdateTypeModel) => {
		return axios.put(`supplier/update/${payload.id}`, payload.data);
	},
	DELETE_SUPPLIER: (id: number) => {
		return axios.delete(`supplier/delete/${id}`);
	}
}

export default supplier;