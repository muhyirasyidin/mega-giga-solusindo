/* 
	Modules
*/

import auth from "./modules/auth";
import barang from './modules/barang';
import supplier from "./modules/supplier";

export default {
	auth,
	barang,
	supplier
}