import '@/config/icon';
import '@/config/axios';
import store from './core/index';

import { createApp } from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';
/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Particles from "vue3-particles";

import App from './App.vue';
import router from './router';

declare global {
	interface Window {
			Swal:any;
	}
}

/* 
	Bootstrap
*/
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";

/**
 * 
 * Sweetalert
 */
import 'sweetalert2/dist/sweetalert2.min.css';

const app: any = createApp(App)

/* register component */
app.component('FontAwesome', FontAwesomeIcon)

app.use(router)
app.use(store)
app.use(VueSweetalert2)
app.use(Particles)

// register for global used
window.Swal =  app.config.globalProperties.$swal;

app.mount('#app')
