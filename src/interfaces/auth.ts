export interface AuthTypeModel {
	user: null | UserTypeModel;
}

export interface UserTypeModel {
	id: number;
	profileName: string;
	token: string;
	username: string;
}