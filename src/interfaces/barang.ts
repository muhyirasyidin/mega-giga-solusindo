import type { ParamsTypeModel } from './common'
import type { SupplierModel } from './supplier';

export interface BarangTypeModel {
	barangParams: ParamsTypeModel;
	barang: BarangModel[];
	barangPagination: BarangPaginationTypeModel;
	selectedBarang: null | BarangModel;
}

export interface BarangModel {
	harga: number;
	id: number;
	namaBarang: string;
	stok: number;
	suplier: SupplierModel;
}

export interface BarangPaginationTypeModel {
	total_page: number;
	total_record: number;
}

