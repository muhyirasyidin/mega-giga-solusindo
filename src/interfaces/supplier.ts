import type { ParamsTypeModel } from './common'

export interface SuplierTypeModel {
	supplierParams: ParamsTypeModel;
	supplier: SupplierModel[];
	supplierPagination: SupplierPaginationTypeModel;
	selectedSupplier: null | SupplierModel;
}

export interface SupplierModel {
	id: number;
	namaSupplier: string;
	noTelp: string;
	alamat: string;
}

export interface SupplierPaginationTypeModel {
	total_page: number;
	total_record: number;
}

