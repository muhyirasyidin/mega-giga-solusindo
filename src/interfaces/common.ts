export interface ParamsTypeModel {
	limit: number;
	offset: number;
	search: string;
}

export interface PayloadUpdateTypeModel {
	id: number;
	data: object;
}