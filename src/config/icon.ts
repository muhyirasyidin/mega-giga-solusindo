/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import specific icons */
import {
	faBoxesStacked,
	faBoxesPacking,
	faRightFromBracket,
	faArrowLeft,
	faArrowRight,
	faPlus,
	faTrash,
	faPencil
} from '@fortawesome/free-solid-svg-icons'

/* add icons to the library */
library.add(faBoxesStacked)
library.add(faBoxesPacking)
library.add(faRightFromBracket)
library.add(faArrowLeft)
library.add(faArrowRight)
library.add(faPlus)
library.add(faTrash)
library.add(faPencil)