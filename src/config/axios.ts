import axios from 'axios';

/**
 * Stores
 */
import store from '@/core/index';

axios.defaults.baseURL = 'http://159.223.57.121:8090/'

axios.defaults.headers.common['Authorization'] = store.state.auth.user ?
	`Bearer ${store.state.auth.user.token}` : '';