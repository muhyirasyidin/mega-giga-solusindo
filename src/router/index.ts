import { createRouter, createWebHistory } from 'vue-router';

/**
 * 
 * Store
 */
import store from '@/core/index'

/* 
	Pages
*/
import AuthView from '@/views/auth/AuthView.vue';
import LoginView from '@/views/auth/LoginView.vue';
import RegisterView from '@/views/auth/RegisterView.vue';
import HomeView from '@/views/home/HomeView.vue';
import BarangView from '@/views/home/BarangView.vue';
import SupplierView from '@/views/home/SupplierView.vue';
import CreateSupplierView from '@/components/form/supplier/CreateView.vue';
import EditSupplierView from '@/components/form/supplier/EditView.vue';
import CreateBarangView from '@/components/form/barang/CreateView.vue';
import EditBarangView from '@/components/form/barang/EditView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
		{
			path: '/',
			redirect: { path: "/auth/login" },
		},
    {
      path: '/auth',
			name: 'auth',
      component: AuthView,
			children: [
				{
					name: 'login',
					path: 'login',
					component: LoginView
				},
				{
					name: 'register',
					path: 'register',
					component: RegisterView
				}
			],
    },
		{
			path: '/home',
			name: 'dashboard',
			redirect: {path: '/home/barang'},
			component: HomeView,
			children: [
				{
					name: 'barang',
					path: 'barang',
					component: BarangView
				},
				{
					name: 'barang-create',
					path: 'barang/barang-create',
					component: CreateBarangView
				},
				{
					name: 'barang-edit',
					path: 'barang/barang-edit/:id',
					component: EditBarangView
				},
				{
					name: 'supplier',
					path: 'supplier',
					component: SupplierView
				},
				{
					name: 'supplier-create',
					path: 'supplier/supplier-create',
					component: CreateSupplierView
				},
				{
					name: 'supplier-edit',
					path: 'supplier/supplier-edit/:id',
					component: EditSupplierView
				},
			],
		}
  ]
})

router.beforeEach(async (to) => {
  if (
    // make sure the user is authenticated
    !store.state.auth.user &&
    // ❗️ Avoid an infinite redirect
    (to.name !== 'login' && to.name !== 'register')
  ) {
    // redirect the user to the login page
    return { name: 'login' }
  }
})

export default router
